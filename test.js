const computersElement = document.getElementById('computers')
const laptopInfoElement = document.getElementById('laptopInfo')
const priceElement = document.getElementById('price')
const largeTextElemet = document.getElementById('largeText')
const inforTextSmallElement = document.getElementById('inforTextSmall')
const laptopImageElement = document.getElementById('laptopImage')
const ImageApiUrl = "https://computer-api-production.up.railway.app/"
const currency = " kr"
const currencyElement = document.getElementById('currency')
currencyElement.innerText = currency
let computers = [];
fetch("https://computer-api-production.up.railway.app/computers")
	.then(response => response.json())
	.then(data => computers = data)
	.then(computers => addcomputersToMenu(computers));
const addcomputersToMenu = (computers) => {
	computers.forEach(x => addcomputerToMenu(x));
	const x = computers[0].price
	const z = computers[0].image
	const imageTag2 = "<img src='" + ImageApiUrl + z + "' width='200px'/>"
	laptopImageElement.innerHTML = imageTag2
	// const y = new Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' }).format(x);
	priceElement.innerText = x
	laptopInfoElement.innerText = computers[0].description
	largeTextElemet.innerText = computers[0].title
	inforTextSmallElement.innerText = computers[0].specs
}
const addcomputerToMenu = (computer) => {
	const computerElement = document.createElement('option')
	computerElement.value = computer.id;
	laptopInfoElement.appendChild(document.createTextNode(computer.description))
	computerElement.appendChild(document.createTextNode(computer.title));
	computersElement.appendChild(computerElement)
}
const handlecomputerMenuChange = e => {
	const selectedcomputer = computers[e.target.selectedIndex]
	const priceElementFormatet = selectedcomputer.price
	// const correctPrice = new Intl.NumberFormat('no-NO', { style: 'currency', currency: 'NOK' }).format(priceElementFormatet);
	priceElement.innerText = priceElementFormatet
	laptopInfoElement.innerText = selectedcomputer.description
	largeTextElemet.innerText = selectedcomputer.title
	inforTextSmallElement.innerText = selectedcomputer.specs
	const imageTagUrl = selectedcomputer.image
	const imageTag = "<img src='" + ImageApiUrl + imageTagUrl + "' width='150px'/>"
	laptopImageElement.innerHTML = imageTag
}
computersElement.addEventListener('change', handlecomputerMenuChange)
const balanceAccountNumberElement = document.getElementById('balance')
const loanButtonElement = document.getElementById('btnLoan')
const outstandingElement = document.getElementById('outstanding')
const workButtonElement = document.getElementById('btnWork')
const workAccountNumberElement = document.getElementById('workAccount')
const bankButtonElement = document.getElementById('btnBank')
const buyButtonElement = document.getElementById('btnBuy')
let balanceAccountNumber = 0
let creditAccountNumber = 0
let workAccountNumber = 0
const wage = 100
//const creditNumber = 0
const updateBalanceAccountNumberElement = (newTotal) => {
	balanceAccountNumberElement.innerText = newTotal + currency
	workAccountNumberElement.innerText = 0 + currency
}
const updateBalanceAccountNumberFromCreditElement = (newBalanceAccountNumberForCredit) => {
	balanceAccountNumberElement.innerText = newBalanceAccountNumberForCredit + currency
}
const updateWorkAccountNumberElement = (newWorkAccountTotal) => {
	workAccountNumberElement.innerText = newWorkAccountTotal + currency
}
const updateCreditAccountNumber = (newCreditAccountNumber) => {
	outstandingElement.innerText = newCreditAccountNumber + currency
	workAccountNumberElement.innerText = 0 + currency
}
const getCreditAccountNumber = () => {
	return creditAccountNumber
}
const getAccountBalance = () => {
	return balanceAccountNumber
}
const getWorkAccountNumber = () => {
	return workAccountNumber
}
const addToWorkAccount = () => {
	workAccountNumber += wage
	return workAccountNumber
}
const addToBalanceAccount = () => {
	console.log('Bank button pressed')
	balanceAccountNumber += workAccountNumber
	workAccountNumber -= workAccountNumber
	return balanceAccountNumber
	return workAccountNumber
}
const add90PercentToWorkAccount = () => {
	workAccountNumber += 90
	return workAccountNumber
}
const addToBalanceAccountFromCredit = (number) => {
	console.log('Adding credit to Balance Account')
	balanceAccountNumber += number
	return balanceAccountNumber
	workAccountNumber -= workAccountNumber
}
const borrowMoney = (number) => {
	console.log(number)
	creditAccountNumber -= number
	console.log(creditAccountNumber)
	return creditAccountNumber
}
const addMoney = (salary) => {
	creditAccountNumber += salary
	workAccountNumber -= workAccountNumber
	return creditAccountNumber
}
const add10PercentMoney = () => {
	creditAccountNumber += 10
	return creditAccountNumber
	return workAccountNumber
}
const buyComputer = (price) => {
	balanceAccountNumber -= price
	return balanceAccountNumber
}
const bank = {
	getAccountBalance,
	addToWorkAccount,
	getWorkAccountNumber,
	addToBalanceAccount,
	borrowMoney,
	getCreditAccountNumber,
	addToBalanceAccountFromCredit,
	addMoney,
	add90PercentToWorkAccount,
	add10PercentMoney,
	buyComputer
}
const handleLoanButtonClicked = () => {
	const loanSum = prompt('Enter the amount you want to borrow: ')
	const number = parseInt(loanSum);
	const doubleNumber = bank.getAccountBalance() * 2
	console.log('You cant borrow more than: ', doubleNumber + currency)
	if (number > doubleNumber) {
		alert('You cant borrow more then ' + doubleNumber + currency)
	} else {
		if (isNaN(number) || number < 0) {
			alert('Please enter positve number')
		} else {
			const a = bank.getCreditAccountNumber()
			if (a < 0) {
				alert('Redeem your loan and try again')
			} else {
				bank.borrowMoney(number)
				let outstandingElementHeading = document.querySelector('#outstanding-heading')
				let outstandingElementContent = document.querySelector('#outstanding')
				outstandingElementHeading.classList.add('visability')
				outstandingElementContent.classList.add('visability')
				const newCreditAccountNumber = bank.getCreditAccountNumber()
				updateCreditAccountNumber(newCreditAccountNumber)
				bank.addToBalanceAccountFromCredit(number)
				const newBalanceAccountNumberForCredit = bank.getAccountBalance()
				console.log(newBalanceAccountNumberForCredit)
				updateBalanceAccountNumberFromCreditElement(newBalanceAccountNumberForCredit)
			}
		}
	}
}
const handleWorkButtonClicked = () => {
	const creditAmount = creditAccountNumber
	const bankButtonElement = document.getElementById('btnBank')
	bankButtonElement.classList.remove('also-hidden')
	if (creditAmount < 0) {
		console.log('Joe has Credit - Add 10% of salary to Credit Account')
		console.log()
		bank.add10PercentMoney()
		const newCreditAccountNumber = bank.getCreditAccountNumber()
		updateCreditAccountNumber(newCreditAccountNumber)
		bank.add90PercentToWorkAccount()
		const newWorkAccountTotal = bank.getWorkAccountNumber()
		updateWorkAccountNumberElement(newWorkAccountTotal)
		console.log(newWorkAccountTotal)
	} else {
		console.log('Joe does not have credit. Add 100% of salary to Balance Account')
		// If Joe does not have credit - Add 100% of salary to balance account
		bank.addToWorkAccount()
		const newWorkAccountTotal = bank.getWorkAccountNumber()
		updateWorkAccountNumberElement(newWorkAccountTotal)
	}
}
const handleBankButtonClicked = () => {
	const bankButtonElement = document.getElementById('btnBank')
	bankButtonElement.classList.add('also-hidden')
	const creditAmount = creditAccountNumber
	if (creditAmount < 0) {
		console.log('Joe has credit')
		const newWorkAccountTotal = bank.getWorkAccountNumber()
		const sum = newWorkAccountTotal + creditAmount
		console.log('The sum is', sum)
		if (sum >= 0) {
			console.log('Enough money to cover the credit')
			const salary = newWorkAccountTotal
			console.log('You own', creditAmount)
			const x = Math.abs(creditAmount)
			bank.addMoney(x)
			const foobar = sum
			bank.addToBalanceAccountFromCredit(sum)
			const newBalanceAccountNumberForCredit = bank.getAccountBalance()
			console.log(newBalanceAccountNumberForCredit)
			updateBalanceAccountNumberFromCreditElement(newBalanceAccountNumberForCredit)
			const newCreditAccountNumber = bank.getCreditAccountNumber()
			updateCreditAccountNumber(newCreditAccountNumber)
		} else {
			const salary = newWorkAccountTotal
			bank.addMoney(salary)
			const newCreditAccountNumber = bank.getCreditAccountNumber()
			updateCreditAccountNumber(newCreditAccountNumber)
		}
	} else {
		console.log('Joe does not have credit')
		bank.addToBalanceAccount()
		const newTotal = bank.getAccountBalance()
		updateBalanceAccountNumberElement(newTotal)
		let outstandingElementHeading = document.querySelector('#outstanding-heading')
		let outstandingElementContent = document.querySelector('#outstanding')
		outstandingElementHeading.classList.remove('visability')
		outstandingElementContent.classList.remove('visability')
	}
}
const handleBuyButtonClicked = () => {
	console.log('Buy button clicked')
	const amount = bank.getAccountBalance()
	console.log('Amount available: ', amount)
	const price = priceElement.innerText
	if (amount >= price) {
		bank.buyComputer(price)
		const newTotal = bank.getAccountBalance()
		updateBalanceAccountNumberElement(newTotal)
		alert('Sucsess! Paying ' + price + currency + ' from your balance account')
	} else {
		const computerNameElement = document.getElementById('largeText')
		const computerName = computerNameElement.innerText
		alert('You dont have anough money to buy "' + computerName + '"')
	}
}
loanButtonElement.addEventListener('click', handleLoanButtonClicked)
workButtonElement.addEventListener('click', handleWorkButtonClicked)
bankButtonElement.addEventListener('click', handleBankButtonClicked)
buyButtonElement.addEventListener('click', handleBuyButtonClicked)